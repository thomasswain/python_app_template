#!/bin/bash

# Set APP_NAME and PYTHON_VERSION in ./install_vars then run this script to set up various files correctly.
source ./install_vars

PYTHON_DIR="/usr/share/${APP_NAME}"
PYTHON_MAIN="${PYTHON_DIR}/${APP_NAME}.py"
EXECUTABLE_DIR="/usr/local/bin"
FULL_EXECUTABLE_NAME="${EXECUTABLE_DIR}/${APP_NAME}"
PYTHON_CMD="python${PYTHON_VERSION}"
PYTHON_VENV_CMD="${PYTHON_DIR}/venv/bin/${PYTHON_CMD}"
ESCAPED_FULL_EXECUTABLE_NAME=$(echo "$EXECUTABLE_DIR/$APP_NAME" | sed 's/\//\\\//g')
ESCAPED_URL=$(echo "$URL" | sed 's/\//\\\//g')

# Set up postinst.sh
sed -i "s/^[#]*\s*APP_NAME=.*/APP_NAME='${APP_NAME}'/" postinst.sh
sed -i "s/^[#]*\s*PYTHON_VERSION=.*/PYTHON_VERSION='${PYTHON_VERSION}'/" postinst.sh

# Name the /usr/local/bin executable file, systemd service file and the /usr/share app directory main Python file, config file
find src/usr/local/bin/ -type f | while IFS= read file_name; do mv "$file_name" "src${EXECUTABLE_DIR}/${APP_NAME}" 2>/dev/null; done
find src/usr/share/ -mindepth 1 -maxdepth 1 -type d | while IFS= read file_name; do mv "$file_name" "src${PYTHON_DIR}" 2>/dev/null; done
find src/usr/share/${APP_NAME} -mindepth 1 -maxdepth 1 -type f -name "*.py" ! -name "*__init__.py"| while IFS= read file_name; do mv "$file_name" "src${PYTHON_MAIN}" 2>/dev/null; done
find src/etc/ -mindepth 1 -maxdepth 1 -type d ! -name system* | while IFS= read file_name; do mv "$file_name" "src/etc/${APP_NAME}" 2>/dev/null; done
find src/etc/${APP_NAME} -type f -name "*.yaml" | while IFS= read file_name; do mv "$file_name" "src/etc/${APP_NAME}/${APP_NAME}.yaml" 2>/dev/null; done
find src/etc/systemd/system -type f -name "*.service" | while IFS= read file_name; do mv "$file_name" "src/etc/systemd/system/${APP_NAME}.service" 2>/dev/null; done

# Set up systemd service file
sed -i "s/^[#]*\s*Description=.*/Description=${DESCRIPTION}/" src/etc/systemd/system/${APP_NAME}.service
sed -i "s/^[#]*\s*ExecStart=.*/ExecStart=${ESCAPED_FULL_EXECUTABLE_NAME}/" src/etc/systemd/system/${APP_NAME}.service
sed -i "s/^[#]*\s*Documentation=.*/Documentation=${ESCAPED_URL}/" src/etc/systemd/system/${APP_NAME}.service

# Set up /usr/local/bin executable file
echo '#!/bin/bash' > src$FULL_EXECUTABLE_NAME
echo '' >> src$FULL_EXECUTABLE_NAME
echo "sudo ${PYTHON_VENV_CMD} ${PYTHON_MAIN}" >> src$FULL_EXECUTABLE_NAME

#!/bin/bash

source ./install_vars

rm -rf *.deb

fpm -s dir \
    -t deb \
    -f \
    -n "${APP_NAME}" \
    -v "${VERSION}" \
    -d "${DEPENDENCIES}" \
    -a all \
    -m "${MAINTAINER}" \
    --deb-no-default-config-files \
    --url "${URL}" \
    --description "${DESCRIPTION}" \
    --after-install postinst.sh \
    --vendor "${VENDOR}" \
    --provides "${APP_NAME}" \
    src/=/ \
    requirements.txt=/usr/share/${APP_NAME}/requirements.txt

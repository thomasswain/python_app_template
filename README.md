# python_app_template

Template for a simple Python app that includes .deb build procedure for Ubuntu/Debian using FPM. Also includes Gitlab CI/CD runner config for automated format/unittest/build/push.

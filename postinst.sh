#!/bin/bash

# Controlled by update_name.sh script
APP_NAME='my-app'
PYTHON_VERSION='3.9'
PYTHON_DIR="/usr/share/${APP_NAME}"
EXECUTABLE_DIR="/usr/local/bin"
PYTHON_CMD="python${PYTHON_VERSION}"

echo "******** Installing Python3 virtualenv..."
pip3 install virtualenv || exit 1

echo "******** Creating Python3 virtual environment at ${PYTHON_DIR}/venv ..."
$PYTHON_CMD -m venv "${PYTHON_DIR}/venv" || exit 2

echo "******** Installing pip3 modules in requirements.txt..."
$PYTHON_DIR/venv/bin/pip3 install -r "${PYTHON_DIR}/requirements.txt" || exit 3
rm "${PYTHON_DIR}/requirements.txt" || exit 4

echo "******** Setting ${EXECUTABLE_DIR}/${APP_NAME} file permissions..."
chmod 755 "${EXECUTABLE_DIR}/${APP_NAME}" || exit 5

echo "******** Creating /var/log/${APP_NAME}/ directory..."
mkdir -p "/var/log/${APP_NAME}" || exit 6

echo "******** Successfully installed. Run program with '${APP_NAME}' command" || exit 7


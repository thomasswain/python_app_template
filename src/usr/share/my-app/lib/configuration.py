import logging
import logging.config
import os
import yaml


class Configuration:
    """Initialise configuration."""

    _config = None

    def __init__(self, config_path=None, load_config=True):
        """Unless already loaded, initialise the configuration by reading YAML config file
        at config_path, /etc/my-app/my-app.yaml or path specified as value of MY_APP_CONFIG
        environment variable."""

        if not Configuration._config:
            Configuration._config = {}
            if load_config:
                config_path = config_path or os.getenv(
                    "MY_APP_CONFIG", "/etc/my-app/my-app.yaml"
                )
                try:
                    with open(config_path, "r") as fp:
                        Configuration._config.update(yaml.safe_load(fp.read()))
                except IOError:
                    logging.warning("error loading configuration %s.", config_path)
                except yaml.YAMLError:
                    logging.warning("error parsing configuration %s.", config_path)

    @staticmethod
    def update(config):
        Configuration._config.update(config)

    def __contains__(self, item):
        return item in Configuration._config

    def __getitem__(self, item):
        return Configuration._config[item]

    def __setitem__(self, item, value):
        Configuration._config[item] = value

    @staticmethod
    def get(item, default):
        return Configuration._config.get(item, default)

    @staticmethod
    def initialise_logging():
        logging.config.dictConfig(Configuration._config["logging"])

    @classmethod
    def reset(cls):
        cls._config = None

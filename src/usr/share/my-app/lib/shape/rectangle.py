from lib.shape import Shape


class Rectangle(Shape):
    """Represents a Rectangle"""

    def __init__(self, width=1, height=1):
        self._name = "Rectangle"
        self._width = width
        self._height = height

    def area(self):
        """Width x Height"""
        return self._width * self._height

    def perimeter(self):
        """2 x Width + 2 x Height"""
        return 2 * self._width + 2 * self._height

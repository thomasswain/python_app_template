import math
from lib.shape import Shape


class Circle(Shape):
    """Represents a Circle"""

    def __init__(self, radius=1):
        self._name = "Circle"
        self._radius = radius

    def area(self):
        """Pi x radius^2"""
        return math.pi * (self._radius ** 2)

    def perimeter(self):
        """Pi x 2 x radius"""
        return math.pi * 2 * self._radius

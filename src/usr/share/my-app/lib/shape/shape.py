class Shape:
    """Represent a general 2D shape"""

    def __init__(self):
        """Abstract"""
        self._name = "Unnamed shape"

    def area(self):
        """Abstract"""

    def perimeter(self):
        """Abstract"""

    @property
    def name(self):
        """Name getter"""
        return self._name

    @name.setter
    def name(self, name):
        """Name setter"""
        self._name = name

    @name.deleter
    def name(self):
        """Name deleter"""
        del self._name

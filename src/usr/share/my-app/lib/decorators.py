import functools
import os
import sys
import time


def requires_root(function):
    """Decorator to require root access for a command."""

    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        if not os.geteuid() == 0:
            print("Can only run as root.", file=sys.stderr)
            sys.exit(1)
        result = function(*args, **kwargs)
        return result

    return wrapper


def rate_limit(func):
    """Rate limit function calls by 1s."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        time.sleep(1)
        return func(*args, **kwargs)

    return wrapper


def rate_limit_custom(delay=0.5):
    """Rate limit function calls by user-defined number of seconds (default 0,5s)."""

    def decorator(function):
        @functools.wraps(
            function
        )  # Preserves information about original function like __name__
        def wrapper(*args, **kwargs):
            time.sleep(delay)
            return function(*args, **kwargs)

        return wrapper

    return decorator


def repeat(num_times):
    """Repeats function call num_times times."""

    def decorator(func):
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value

        return wrapper_repeat

    return decorator


# Decorator Templates
def general_decorator(function):
    """General decorator with no arguments, to be used as a template."""

    @functools.wraps(
        function
    )  # Preserves information about original function like __name__
    def wrapper(*args, **kwargs):
        result = function(*args, **kwargs)
        return result

    return wrapper


def general_decorator_with_args(arguments):
    """General decorator with arguments, to be used as a template. One additional function wrapper that handles arguments."""

    def decorator(function):
        @functools.wraps(
            function
        )  # Preserves information about original function like __name__
        def wrapper(*args, **kwargs):
            handle_input = arguments
            result = function(*args, **kwargs)
            return result

        return wrapper

    return decorator

import logging
import cowsay
import time
from lib.decorators import requires_root
from lib.decorators import repeat, requires_root, rate_limit_custom
from lib.shape import Rectangle, Circle
from lib.configuration import Configuration


# @repeat(num_times=5)
# @rate_limit_custom(delay=2)
# @requires_root
def display_message(message):
    """Display message using Cowsay."""
    cowsay.tux(message)


if __name__ == "__main__":
    # Load config
    configuration = Configuration()

    # Read from config
    welcome_message = configuration.get("message", "Fallback message!!!")

    # Initialise logging
    configuration.initialise_logging()

    # Log the start of the app using root logger config
    general_logger = logging.getLogger(__name__)
    general_logger.debug("my-app started ...")
    general_logger.info("my-app started ...")

    # Log the start of the app using specific logger config
    specific_logger = logging.getLogger("specific.logger")
    specific_logger.debug("my-app started ...")
    specific_logger.info("my-app started ...")

    # Create 2 different shapes using custom classes
    rectangle = Rectangle(2, 5)
    circle = Circle(3)

    # Display message from config and results from class methods using Cowsay
    display_message(
        f"{rectangle.name}: perimeter {rectangle.perimeter():.2f}, area {rectangle.area():.2f}"
        f"\n{circle.name}: perimeter {circle.perimeter():.2f}, area {circle.area():.2f}"
        f"\n{welcome_message}"
    )

    # Start running something like a server/listener/daemon (can use app in a systemd service)
    print("Starting server/listener/daemon ...")
    while True:
        time.sleep(5)

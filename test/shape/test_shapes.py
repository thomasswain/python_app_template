import os, sys
import pytest

sys.path.append(
    os.path.dirname(os.path.realpath(__file__)) + "/../../src/usr/share/my-app"
)

from lib.shape.shape import Shape
from lib.shape.rectangle import Rectangle
from lib.shape.circle import Circle


@pytest.fixture()
def setup_abstract_shape():
    return Shape()


@pytest.fixture()
def setup_circle():
    return Circle(5.467)


@pytest.fixture()
def setup_rectangle():
    return Rectangle(3.953, 1.437)


# Tests for abstract Shape
def test_abstract_shape_name(setup_abstract_shape):
    assert setup_abstract_shape.name == "Unnamed shape"


def test_abstract_shape_area(setup_abstract_shape):
    assert setup_abstract_shape.area() is None


def test_abstract_shape_perimeter(setup_abstract_shape):
    assert setup_abstract_shape.perimeter() is None


# Tests for Circle shape
def test_circle_name(setup_circle):
    assert setup_circle.name == "Circle"


def test_circle_area(setup_circle):
    assert setup_circle.area() == pytest.approx(93.8962)


def test_circle_perimeter(setup_circle):
    assert setup_circle.perimeter() == pytest.approx(34.3502)


# Tests for Rectangle shape
def test_rectangle_name(setup_rectangle):
    assert setup_rectangle.name == "Rectangle"


def test_rectangle_area(setup_rectangle):
    assert setup_rectangle.area() == pytest.approx(5.68046)


def test_rectangle_perimeter(setup_rectangle):
    assert setup_rectangle.perimeter() == pytest.approx(10.78)
